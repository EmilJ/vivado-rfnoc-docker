## About

This dockerfile gives you Vivado in Ubuntu 16.04 LTS, providing you with a clean, replicable, isolated sandbox. 
Keep in mind, that docker containers don't have persistent storage other than the mounted directory. 
The build takes over 15 minutes (6-core Ryzen 3600, NVMe SSD) and requires about 100GB free space. After build,
the image takes up 50GB.

## Prep
Download 
`Xilinx_Vivado_SDK_2019.1_0524_1430.tar.1.gz` from Xilinx and put it next to Dockerfile, do the same for the 2019.1.1 Vivado update archive.
Also unzip the AR73068 patch so that a directory named AR73068 is next to the Dockerfile. You will need `docker`, and
`nvidia-docker` if you want GUI on nvidia. Untested with AMD GPUs, maybe try
[this?](https://techviewleo.com/run-gui-applications-in-docker-using-x11docker/)


## Setup

Here's where this dockerfile differs from those of laysakura or phwl: To build the image, you need user permissions
to use pip safely. To make your user work with GUI output, you need to use gosu to switch to it.
This combination means you need to provide your UID, and the built image won't be very portable. Also, modify
the install_config.txt file to select your desired supported chip families.

```bash
docker build . -t rfnoc --build-arg USER_ID=$(id -u $USERNAME)
```

This might be unsafe, but it allows the container to pass data to your X11 session to run Vivado's graphical interface.

```bash
xhost +
```

Now, if you are running Nvidia graphics, as long as you have `nvidia-docker` installed:

```bash
docker run --runtime=nvidia -e DISPLAY -e USER_ID=$(id -u $USERNAME) -it --rm -v $PWD/..:/home/user/work -v /tmp/.X11-unix:/tmp/.X11-unix -w /home/user rfnoc
```

should drop you into bash as user `user` and if you type `vivado`, GUI should appear. If you don't want that,
remove the `/tmp/.X11-unix` mountpoint, and `--runtime` option. The command also mounts the parent directory
of this one as `/home/user/work` in the guest. Feel free to change `$PWD/..` to any other path, or add
more mount points.
Have fun!

## Cleanup

If you are happy with how this works, remove unused images and containers, this won't break your final image:

```bash
docker image prune
docker container prune
```

## Inspiration

[phwl](https://phwl.org/2020/xilinx-vivado-on-ubuntu-using-docker/)

[laysakura](https://blog.myon.info/entry/2018/09/15/install-xilinx-tools-into-docker-container/)


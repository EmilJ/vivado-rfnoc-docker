FROM ubuntu:bionic

ENV DEBIAN_FRONTEND noninteractive

ARG INSTALL_FILE="Xilinx_Vivado_SDK_2019.1_0524_1430.tar.1.gz"
ARG UPDATE_FILE="Xilinx_Vivado_SDx_Update_2019.1.1_0629_0743.tar.gz"
ARG gosu_version=1.10

# apt dependencies
RUN \
  apt-get update -y && \
  apt-get upgrade -y && \
  apt-get -y --no-install-recommends install \
    ca-certificates curl sudo xorg dbus dbus-x11 ubuntu-gnome-default-settings gtk2-engines \
    ttf-ubuntu-font-family fonts-ubuntu-font-family-console fonts-droid-fallback lxappearance \
    git python-setuptools python-dev python python3-pip autotools-dev python-mako python-six && \
  apt-get -y --no-install-recommends install build-essential && \
  apt-get autoclean && \
  apt-get autoremove && \
  rm -rf /var/lib/apt/lists/* && \
  echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
# gosu is a user account creator for docker
  curl -SL "https://github.com/tianon/gosu/releases/download/${gosu_version}/gosu-$(dpkg --print-architecture)" \
    -o /usr/local/bin/gosu && \
  rm -rf /usr/local/bin/gosu.asc /root/.gnupg && \
  chmod +x /usr/local/bin/gosu

# entrypoint
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
ARG USER_ID
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
ENV PATH /home/user/.local/bin:$PATH
COPY install_config.txt /vivado-installer/
COPY update_config.txt /vivado-updater/
COPY ${INSTALL_FILE} /vivado-installer/
COPY ${UPDATE_FILE} /vivado-updater/

RUN \
  useradd -m -s /bin/bash -u "$USER_ID" -o -d /home/user user && \
  usermod -aG sudo user && \
  chown user:user -R /home/user && \
# pybomb dependencies
  /usr/local/bin/gosu user /bin/bash -c "whoami && \
  python3 -m pip install --user --upgrade pip && \
  python3 -m pip install --user setuptools && \
  python3 -m pip install --user typing && \
  python3 -m pip install --user requests && \
  python3 -m pip install --user numpy && \
  python3 -m pip install --user git+https://github.com/gnuradio/pybombs.git" && \
# vivado dependencies
  dpkg --add-architecture i386 && \
  apt-get update && \
  apt-get -y --no-install-recommends install \
    build-essential git gcc-multilib libc6-dev:i386 ocl-icd-opencl-dev libjpeg62-dev && \
  apt-get autoclean && \
  apt-get autoremove && \
  rm -rf /var/lib/apt/lists/* && \
# install vivado
  cat /vivado-installer/${INSTALL_FILE} | tar zx --strip-components=1 -C /vivado-installer && \
  /vivado-installer/xsetup \
    --agree 3rdPartyEULA,WebTalkTerms,XilinxEULA \
    --batch Install \
    --config /vivado-installer/install_config.txt && \
  rm -rf /vivado-installer && \
# update vivado
  cat /vivado-updater/${UPDATE_FILE} | tar zx --strip-components=1 -C /vivado-updater && \
  /vivado-updater/xsetup \
    --agree 3rdPartyEULA,WebTalkTerms,XilinxEULA \
    --batch Install \
    --config /vivado-updater/update_config.txt && \
#  mkdir -p /tools/Xilinx/Vivado/2019.1/patches/AR73068 && \
  rm -rf /vivado-updater


# patch vivado
ADD AR73068 /tools/Xilinx/Vivado/2019.1/patches/AR73068

# TODO UART

CMD ["/bin/bash", "-l"]

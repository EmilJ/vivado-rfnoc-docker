#!/bin/bash

UART_GROUP_ID=${UART_GROUP_ID:-20}
if ! grep -q "x:${UART_GROUP_ID}:" /etc/group; then
  groupadd -g "$UART_GROUP_ID" uart
fi
UART_GROUP=$(grep -Po "^\\w+(?=:x:${UART_GROUP_ID}:)" /etc/group)

if [[ -n "$USER_ID" ]]; then
  usermod -aG "$UART_GROUP" user
  chown user $(tty)
  echo 'source /tools/Xilinx/Vivado/2019.1/settings64.sh' > /home/user/.bash_profile
  exec /usr/local/bin/gosu user "$@"
else
  exec "$@"
fi

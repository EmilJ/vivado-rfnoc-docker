#!/bin/bash
docker run --runtime=nvidia -e DISPLAY -e USER_ID=$(id -u $USERNAME) -e LM_LICENSE_FILE=1717@moon.fit.cvut.cz -e XILINXD_LICENSE_FILE=1717@moon.fit.cvut.cz -it --rm -v $PWD/..:/home/user/work -v /tmp/.X11-unix:/tmp/.X11-unix -w /home/user rfnoc
